"use strict";

var _core = require("@dogmalang/core");

module.exports = exports = {
  ["embedded"]: _core.dogma.use(require("./EmbeddedGen")),
  ["npm"]: _core.dogma.use(require("./NpmGen")),
  ["dflt"]: _core.dogma.use(require("./NpmGen"))
};