"use strict";

var _core = require("@dogmalang/core");

var _core2 = require("@ethronjs/core.gen");

const path = _core.dogma.use(require("@dogmalang/path"));

const $NpmGen = class NpmGen extends _core2.HandlebarsGen {
  constructor() {
    super(...arguments);
    {}
  }

};
const NpmGen = new Proxy($NpmGen, {
  apply(receiver, self, args) {
    return new $NpmGen(...args);
  }

});
module.exports = exports = NpmGen;
const Self = NpmGen;
Object.defineProperty(Self.prototype, "desc", {
  enum: true,
  get: function () {
    {
      return "Generate a NPM catalog.";
    }
  }
});
Object.defineProperty(Self.prototype, "params", {
  enum: true,
  get: function () {
    {
      return {
        ["author"]: "Author name",
        ["authorEmail"]: "Author email",
        ["authorHomepage"]: "Author homepage",
        ["bugsHomepage"]: "Bugs homepage",
        ["bugsEmail"]: "Bugs email",
        ["contributor"]: "Contributor name",
        ["contributorEmail"]: "Contributor email",
        ["contributorHomepage"]: "Contributor homepage",
        ["desc"]: "Catalog description",
        ["gitUrl"]: "Git repo URL",
        ["gitInit"]: {
          ["title"]: "Run 'git init'",
          ["dflt"]: true
        },
        ["gitRemoteAddOrigin"]: {
          ["title"]: "Run 'git remote add origin'",
          ["dflt"]: true
        },
        ["gitConfigLocal"]: {
          ["title"]: "Run 'git config --local user.name'",
          ["dflt"]: true
        },
        ["gitConfigUserName"]: "Git user.name",
        ["homepage"]: "Catalog homepage",
        ["name"]: {
          ["title"]: "Catalog name",
          ["dflt"]: path.name(_core.ps.workDir)
        },
        ["npmInstall"]: {
          ["title"]: "Run 'npm install'",
          ["dflt"]: false
        },
        ["npmWho"]: "NPM username"
      };
    }
  }
});

Self.prototype.preprompt = async function () {
  {
    0, await this.checkDst();
  }
};

Self.prototype.prompt = async function (answers) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("answers", answers, null);

  {
    0, await this.input("name");
    0, await this.input("desc");
    0, await this.input("homepage");

    if (0, await this.input("author")) {
      0, await this.input("authorEmail");
      0, await this.input("authorHomepage");
    }

    if (0, await this.input("contributor")) {
      0, await this.input("contributorEmail");
      0, await this.input("contributorHomepage");
    }

    0, await this.input("npmWho");
    0, await this.confirm("npmInstall");

    if (0, await this.input("gitUrl")) {
      if (0, await this.confirm("gitInit")) {
        0, await this.confirm("gitRemoteAddOrigin");
      }

      if (0, await this.confirm("gitConfigLocal")) {
        0, await this.input("gitConfigUserName");
      }
    }

    0, await this.input("bugsHomepage");
    0, await this.input("bugsEmail");
  }
};

Self.prototype.pregenerate = async function (answers) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("answers", answers, _core.map);

  let {
    name
  } = answers;
  {
    0, await this.checkDst();

    if (_core.dogma.notLike(name, ["@ethroncat/", "ethron-cat-"])) {
      answers.name = "ethron-cat-" + name;
    }
  }
};

Self.prototype.generate = async function (answers) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("answers", answers, null);

  {
    0, await this.fs.cp("_editorconfig", ".editorconfig");
    0, await this.fs.cp("_eslintignore", ".eslintignore");
    0, await this.fs.cp("_eslintrc", ".eslintrc");
    0, await this.fs.cp("_gitignore", ".gitignore");
    0, await this.fs.cp("_npmrc", ".npmrc");
    0, await this.tmpl.render("package.npm.json", answers, "package.json");
    0, await this.tmpl.render("Ethron.npm-cat.js", answers);
    0, await this.tmpl.render("README.md", answers, "README.md");
    0, await this.fs.cp("index.js");
    0, await this.fs.mkdir("test/unit");

    if (answers.gitInit) {
      0, await this.exec("git init");
    }

    if (answers.gitRemoteAddOrigin) {
      0, await this.exec("git remote add origin " + answers.gitUrl);
    }

    if (answers.gitConfigLocal) {
      0, await this.exec("git config --local user.name " + answers.gitConfigUserName);
      0, await this.exec("git config --local user.email " + answers.contributorEmail);
    }

    if (answers.npmInstall) {
      0, await this.exec("npm install");
    }
  }
};