"use strict";

var _core = require("@dogmalang/core");

var _core2 = require("@ethronjs/core.gen");

const path = _core.dogma.use(require("@dogmalang/path"));

const $EmbeddedGen = class EmbeddedGen extends _core2.HandlebarsGen {
  constructor() {
    super(...arguments);
    {}
  }

};
const EmbeddedGen = new Proxy($EmbeddedGen, {
  apply(receiver, self, args) {
    return new $EmbeddedGen(...args);
  }

});
module.exports = exports = EmbeddedGen;
const Self = EmbeddedGen;
Object.defineProperty(Self.prototype, "desc", {
  enum: true,
  get: function () {
    {
      return "Generate an embedded catalog file.";
    }
  }
});
Object.defineProperty(Self.prototype, "params", {
  enum: true,
  get: function () {
    {
      return {
        ["eslintrc"]: {
          ["title"]: "Would you like to generate the .eslintrc file?",
          ["dflt"]: true
        },
        ["cat"]: {
          ["title"]: "Catalog name",
          ["dflt"]: "cat"
        },
        ["packagejson"]: {
          ["title"]: "Would you like to generate the package.json file?",
          ["dflt"]: true
        },
        ["npmInstall"]: {
          ["title"]: "Run 'npm install'",
          ["dflt"]: false
        },
        ["npmrc"]: {
          ["title"]: "Would you like to generate the .npmrc file?",
          ["dflt"]: false
        }
      };
    }
  }
});

Self.prototype.prompt = async function (answers) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("answers", answers, null);

  {
    0, await this.input("cat");

    if (0, await this.confirm("packagejson")) {
      0, await this.confirm("npmrc");
      0, await this.confirm("npmInstall");
    }

    0, await this.confirm("eslintrc");
  }
};

Self.prototype.pregenerate = async function () {
  {
    0, await this.checkDst();
  }
};

Self.prototype.generate = async function (answers) {
  /* istanbul ignore next */
  _core.dogma.paramExpected("answers", answers, null);

  {
    0, await this.fs.cp("Ethron.embedded-cat.js", `Ethron.${answers.cat}.js`);

    if (answers.eslintrc) {
      0, await this.fs.cp("_eslintignore", ".eslintignore");
      0, await this.fs.cp("_eslintrc", ".eslintrc");
    }

    if (answers.packagejson) {
      0, await this.fs.cp("package.embedded.json", "package.json");
      0, await this.fs.cp("_npmrc", ".npmrc");
    }

    if (answers.npmInstall) {
      0, await this.exec("npm i");
    }
  }
};