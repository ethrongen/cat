# @ethrongen/cat

[![NPM version](https://img.shields.io/npm/v/@ethrongen/cat.svg)](https://npmjs.org/package/@ethrongen/cat)
[![Total downloads](https://img.shields.io/npm/dt/@ethrongen/cat.svg)](https://npmjs.org/package/@ethrongen/cat)

[Ethron.js](http://ethronlabs.com) generator for Ethron.js catalogs.

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

*Engineered in Valencia, Spain, EU by EthronLabs.*
