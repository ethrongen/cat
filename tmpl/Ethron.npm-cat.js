//imports
const {cat} = require("ethron");
const eslint = require("@ethronpi/eslint");
const npm = require("@ethronpi/npm");

//Package name.
const pkg = require("./package.json").name;

//Who can publish the package on NPM.
const who = "{{npmWho}}";

//catalog
cat.macro("lint", [
  [eslint, "."]
]).title("Check source code");

// cat.macro("dflt", [
//
// ]).title("Default task");

cat.call("pub", npm.publish, {
  who,
  access: "public",
  path: "."
}).title("Publish on NPM");

cat.call("install", npm.install, {
  pkg: "."
}).title("Install the catalog globally");
